--������ �.�.
-- 8 ������

/*drop TABLE SRC
/
*/
create table SRC(
       ID NUMBER(15), 
       FROM_SRC  VARCHAR2(500),
       TO_SRC  VARCHAR2(500)
)
/

insert into SRC (ID, FROM_SRC, TO_SRC)
  values(1, 100, 101);
/  
insert into SRC (ID, FROM_SRC, TO_SRC)
  values(1, 100, 102);
/
insert into SRC (ID, FROM_SRC, TO_SRC)
  values(1, 100, 103);
/
insert into SRC (ID, FROM_SRC, TO_SRC)
  values(1, 101, 102);
/  
insert into SRC (ID, FROM_SRC, TO_SRC)
  values(1, 101, 103);
/  
insert into SRC (ID, FROM_SRC, TO_SRC)
  values(1, 102, 103);
/
commit
/
select DISTINCT(FROM_SRC)
from SRC
UNION 
select DISTINCT(TO_SRC)
from SRC
