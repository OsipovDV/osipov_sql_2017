--������ �.�.
-- 7 ������
/*
drop TABLE GRATH_SRC
/
drop TABLE TEMP_GRATH
/
*/
create table GRATH_SRC(
       ID NUMBER(10),
       FROM_SRC NUMBER(10),        
       TO_SRC   NUMBER(10), 
       Weighte_src NUMBER(10)
)
/
create table TEMP_GRATH(
       FROM_TEMP NUMBER(10),        
       TO_TEMP   NUMBER(10), 
       Weighte NUMBER(10)
)
/
-----------------------------------------------------------
insert into GRATH_SRC (ID, FROM_SRC, TO_SRC, Weighte_src)
  values(1, 100, 102, 100);
/  
insert into GRATH_SRC (ID, FROM_SRC, TO_SRC, Weighte_src)
  values(2, 100, 101, 400);
/ 
insert into GRATH_SRC (ID, FROM_SRC, TO_SRC, Weighte_src)
  values(3, 101, 102, 200);
/ 
insert into GRATH_SRC (ID, FROM_SRC, TO_SRC, Weighte_src)
  values(4, 300, 301, 300);
/ 
insert into GRATH_SRC (ID, FROM_SRC, TO_SRC, Weighte_src)
  values(5, 300, 302, 400);
/ 
insert into GRATH_SRC (ID, FROM_SRC, TO_SRC, Weighte_src)
  values(6, 301, 302, 500);
/ 

------------------------------------------------------------
insert into TEMP_GRATH (FROM_TEMP, TO_TEMP, Weighte)
  values(100, 101, 200);
/ 
insert into TEMP_GRATH (FROM_TEMP, TO_TEMP, Weighte)
  values(200, 201, 200);
/ 
insert into TEMP_GRATH (FROM_TEMP, TO_TEMP, Weighte)
  values(300, 301, 300);
/ 
insert into TEMP_GRATH (FROM_TEMP, TO_TEMP, Weighte)
  values(300, 302, 400);
/ 
insert into TEMP_GRATH (FROM_TEMP, TO_TEMP, Weighte)
  values(301, 302, 600);
/ 
commit
/
select distinct from_, to_, weighte_
from(
  SELECT g.id, t.from_temp, t.to_temp, t.weighte, g.from_src as from_, g.to_src as to_, g.weighte_src as weighte_
  FROM
    GRATH_SRC g
    FULL OUTER JOIN
    TEMP_GRATH t
    ON 
    t.to_temp NOT IN(
       g.to_src
    )
)


---------1 ������� ����������� ���������----------------------------------

SELECT distinct *-- from_src as from_, to_src as to_, weighte_src as weighte_  
FROM
  GRATH_SRC g
  FULL OUTER JOIN
  TEMP_GRATH t
  ON 
  g.from_src = t.from_temp and  g.to_src = t.to_temp
where weighte_src <> weighte    
or
from_temp is null   
or
from_src is null   

---------2 �������(�� ��������, �� � ��������� ��� ���� UNION ALL)---------
--���������� � ���������
SELECT distinct from_src as from_, to_src as to_, weighte_src as weighte_  
FROM
  GRATH_SRC g
  FULL OUTER JOIN
  TEMP_GRATH t
  ON 
  g.from_src = t.from_temp and  g.to_src = t.to_temp
where weighte_src <> weighte    
or
from_temp is null 
UNION ALL
--�����
SELECT distinct from_temp, to_temp, weighte
FROM
  GRATH_SRC g
  FULL OUTER JOIN
  TEMP_GRATH t
  ON 
  g.from_src = t.from_temp and  g.to_src = t.to_temp
where from_src is null 
